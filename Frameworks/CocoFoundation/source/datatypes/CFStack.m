//
//  CFStack.h
//  CocoFoundation
//
//  Created by Thomas Bonk on 18.01.05.
//  Copyright 2005 Thomas Bonk. All rights reserved.
//

#import "CFStack.h"


@implementation CFStack

#pragma mark Initialization / destruction

+ (CFStack*)stack {
	
	return [[[CFStack alloc] init] autorelease];	
}

- (id)init {
	
	if( self = [super init] ) {
		
		mutableArray = [[NSMutableArray alloc] init];
	}
	
	return self;
}


- (void)dealloc {
	
	[mutableArray release];
	[super dealloc];
}


#pragma mark Operations

- (int)size {
	
	return [mutableArray count];
}


- (void)push:(id)anObject {
	
	[mutableArray addObject:anObject];
}


- (id)pop {
	
	id anObject = [mutableArray objectAtIndex:([self size] - 1)];
	
	[mutableArray removeLastObject];
	
	return anObject;
}


- (id)topOfStack {
	
	return [mutableArray objectAtIndex:([self size] - 1)];
}

@end
