//
//  CFIniProperties.m
//
//  Created by Thomas Bonk on 08.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//

#import "CFIniProperties.h"
#import "CFMutableString_Extensions.h"


@interface CFIniProperties (private)

#pragma mark Parsing data

- (void)parseData:(NSData*)theData;
- (NSArray*)linesFromString:(NSString*)aString;
- (NSString*)stripLine:(NSString*)aLine;

- (BOOL)extractSection:(NSString**)aSection fromLine:(NSString*)aLine;
- (BOOL)extractKey:(NSString**)aKey andValue:(NSString**)aValue fromLine:(NSString*)aLine;

@end



@implementation CFIniProperties

#pragma mark Convenience methods

+ (CFIniProperties*)IniFile {
	
	CFIniProperties* iniFile = [[CFIniProperties alloc] init];
	
	return [iniFile autorelease];
}

+ (CFIniProperties*)IniFileWithData:(NSData*)theData {
	
	CFIniProperties* iniFile = [[CFIniProperties alloc] initWithData:theData];
	
	return [iniFile autorelease];
}


#pragma mark Construction/Destruction

- (id)init {
	
	if (self = [super init]) {
		
		_sections = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}

- (id)initWithData:(NSData*)theData {
	
	if (self = [super init]) {
		
		_sections = [[NSMutableDictionary alloc] init];
		
		[self parseData:theData];
	}
	
	return self;
}

- (void)dealloc {

	[_sections release];
	_sections = nil;
	
	[super dealloc];
}


#pragma mark Getting and setting data

- (void)addSection:(NSString*)aSection {
	
	if ([_sections objectForKey:aSection] == nil) {
		
		[_sections setObject:[NSMutableDictionary dictionary] forKey:aSection];
	}
}

- (NSArray*)sections {
	
	return [_sections allKeys];
}

- (void)addKey:(NSString*)aKey andValue:(NSString*)aValue toSection:(NSString*)aSection {
	
	NSMutableDictionary* keyValues = nil;
	
	if ((keyValues = [_sections objectForKey:aSection]) == nil) {
		
		[self addSection:aSection];
		keyValues = [_sections objectForKey:aSection];
	}
	
	[keyValues setObject:aValue forKey:aKey];
}

- (BOOL)stringValue:(NSString**)aString forKey:(NSString*)aKey ofSection:(NSString*)aSection {

	BOOL                 rc        = NO;
	NSMutableDictionary* keyValues = nil;
	
	if ((keyValues = [_sections objectForKey:aSection]) != nil) {
		
		*aString = (NSString*)[keyValues objectForKey:aKey];
		
		rc = (*aString != nil);
	}
	
	return rc;
}

- (BOOL)intValue:(int*)anInt forKey:(NSString*)aKey ofSection:(NSString*)aSection {
	
	BOOL      rc      = NO;
	NSString* aString = nil;
	
	if ((rc = [self stringValue:&aString forKey:aKey ofSection:aSection]) == YES) {
	
		*anInt = [aString intValue];
	}
	
	return rc;
}

- (BOOL)doubleValue:(double*)aDouble forKey:(NSString*)aKey ofSection:(NSString*)aSection {
	
	BOOL      rc      = NO;
	NSString* aString = nil;
	
	if ((rc = [self stringValue:&aString forKey:aKey ofSection:aSection]) == YES) {
		
		*aDouble = [aString doubleValue];
	}
	
	return rc;
}


#pragma mark Parsing data

- (void)parseData:(NSData*)theData {
	
	NSString* iniString      = [[[NSString alloc] initWithData:theData 
													  encoding:NSASCIIStringEncoding] autorelease];
	NSArray*  lines          = [self linesFromString:iniString];
	NSString* theLastSection = @"";
	
	for (int i = 0, n = [lines count]; i < n; i++) {
	
		NSString* line     = [self stripLine:[lines objectAtIndex:i]];
		NSString* theKey   = nil;
		NSString* theValue = nil;
		
		if ([self extractKey:&theKey andValue:&theValue fromLine:line]) {
			
			[self addKey:theKey andValue:theValue toSection:theLastSection];
		}
		else if ([self extractSection:&theLastSection fromLine:line]) {
			
			[self addSection:theLastSection];
		}
		else if ([line length] > 0) {
		
			NSException* ex = [NSException exceptionWithName:@"IniPropertiesParseException"
													  reason:[NSString stringWithFormat:@"Invalid INI-file; parse error in line %d.", (i+1)]
													userInfo:nil];
			
			@throw ex;
		}
	}
}

- (NSArray*)linesFromString:(NSString*)aString {
	
	NSString* separator = nil;

	if ([aString rangeOfString:@"\r\n"].location != NSNotFound) {
		
		separator = @"\r\n";
	}
	else if ([aString rangeOfString:@"\n"].location != NSNotFound) {
		
		separator = @"\n";
	}
	else if ([aString rangeOfString:@"\r"].location != NSNotFound) {
		
		separator = @"\r";
	}
	else {
		
		return [NSArray arrayWithObject:aString];
	}
	
	return [aString componentsSeparatedByString:separator];
}

- (NSString*)stripLine:(NSString*)aLine {

	NSMutableString* str = [NSMutableString stringWithString:aLine];
	NSRange          loc;
	
	// strip whitespaces
	[str trim];
	
	// strip comments
	if ((loc = [str rangeOfString:@"#"]).location != NSNotFound) {
	
		[str deleteCharactersInRange:loc];
	}
	
	if ((loc = [str rangeOfString:@";"]).location != NSNotFound) {
		
		[str deleteCharactersInRange:loc];
	}
	
	return [NSString stringWithString:str];
}


- (BOOL)extractSection:(NSString**)aSection fromLine:(NSString*)aLine {
	
	NSRange pos1, pos2;
	
	if ((pos1 = [aLine rangeOfString:@"["]).location != NSNotFound && (pos2 = [aLine rangeOfString:@"]"]).location != NSNotFound && pos1.location < pos2.location && aSection != nil) {
	
		NSMutableString* str = [NSMutableString stringWithString:[aLine substringWithRange:NSMakeRange(pos1.location + 1, pos2.location - pos1.location - 1)]];
		
		[str trim];
		*aSection = [NSString stringWithString:str];
		
		return YES;
	}
	
	return NO;
}

- (BOOL)extractKey:(NSString**)aKey andValue:(NSString**)aValue fromLine:(NSString*)aLine {
	
	NSRange pos;
	
	if ((pos = [aLine rangeOfString:@"="]).location != NSNotFound && aKey != nil && aValue != nil) {
	
		NSMutableString* key   = [NSMutableString stringWithString:[aLine substringToIndex:pos.location]];
		NSMutableString* value = [NSMutableString stringWithString:[aLine substringFromIndex:pos.location+1]];
		
		[key trim];
		[value trim];
		
		*aKey = [NSString stringWithString:key];
		*aValue = [NSString stringWithString:value];
		
		return YES;
	}
	
	return NO;
}

@end
