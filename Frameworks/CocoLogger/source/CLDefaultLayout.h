//
//  CLDefaultLayout.h
//  CocoLogger
//
//  Created by Thomas Bonk on 27.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@protocol CLLayoutProtocol;
@class CLLogEvent;


@interface CLDefaultLayout : NSObject<CLLayoutProtocol> {

	BOOL _showTimestamp;
}

/** This method initializes the layout with the given parameters.
 * \param theParameters The parameters for this object.
 * \return The initialized instance.
 */
- (id)initWithParameters:(NSDictionary*)theParameters;

/** This method converts a log event to a string.
 * \param aLogEvent The log event to be converted to a string.
 * \return The log event formated to a string.
 */
- (NSString*)logEventToString:(CLLogEvent*)aLogEvent;




@end
