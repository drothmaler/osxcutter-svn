//
//  CLNSLogAppender.h
//  CocoLogger
//
//  Created by Thomas Bonk on 27.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@protocol CLAppenderProtocol;


@interface CLNSLogAppender : NSObject<CLAppenderProtocol> {

}

/** This method initializes the Appender with the given parameters.
 * \param theParameters The parameters for this object.
 * \return The initialized instance.
 */
- (id)initWithParameters:(NSDictionary*)theParameters;

/** This method deallocates the object.
 */
- (void)dealloc;

/** Open the appender.
 *  \return true if opening the appender succeeded and false in case of an 
 *          error.
 */
- (bool)open;

/** Close the appender.
 *  \return true if closing the appender succeeded and false in case of an 
 *          error.
 */
- (bool)close;

/** Flush the appender.
 *  \return true if flushing the appender succeeded and false in case of an 
 *          error.
 */
- (bool)flush;

/** Write a log event. While writing the log event, the thread will be blocked.
 * \param aMessage The message as string that will be logged.
 */
- (bool)log:(NSString*)aMessage;

@end
