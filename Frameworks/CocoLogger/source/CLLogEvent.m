//
//  CLLogEvent.m
//  CocoLogger
//
//  Created by Thomas Bonk on 03.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import "CLLogEvent.h"


@implementation CLLogEvent

#pragma mark Initialization / Deallocation

+ (CLLogEvent*)logEventWithCategory:(NSString*)aCategory 
					  priorityLevel:(CLPriorityLevel)aPrioriyLevel 
							message:(NSString*)aMessage 
							logDate:(NSDate*)aLogDate {

	CLLogEvent* logEvent = [[CLLogEvent alloc] initWithCategory:aCategory 
												  priorityLevel:aPrioriyLevel
														message:aMessage
														logDate:aLogDate];
	
	return [logEvent autorelease];
}

- (id)init {
	
    if (self = [super init]) {
		
        _category = nil;
        _prioriyLevel = NOTSET;
        _message = nil;
        _logDate = nil;
    }
	
    return self;
}

- (id)initWithCategory:(NSString*)aCategory 
		 priorityLevel:(CLPriorityLevel)aPrioriyLevel 
			   message:(NSString*)aMessage 
			   logDate:(NSDate*)aLogDate {
	
	if (self = [super init]) {
		
        _category = [aCategory retain];
        _prioriyLevel = aPrioriyLevel;
        _message = [aMessage retain];
        _logDate = [aLogDate retain];
    }
	
    return self;
}

- (void)dealloc {
	
    [self setCategory:nil];
    [self setMessage:nil];
    [self setLogDate:nil];
    [super dealloc];
}




//=========================================================== 
//  category 
//=========================================================== 
- (NSString *)category {
	
    return _category; 
}

- (void)setCategory:(NSString *)aCategory {
	
    if (_category != aCategory) {
		
        [aCategory retain];
        [_category release];
        _category = aCategory;
    }
}

//=========================================================== 
//  prioriyLevel 
//=========================================================== 
- (CLPriorityLevel)prioriyLevel {
	
    return _prioriyLevel;
}

- (void)setPrioriyLevel:(CLPriorityLevel)aPrioriyLevel {
	
    _prioriyLevel = aPrioriyLevel;
}

//=========================================================== 
//  message 
//=========================================================== 
- (NSString *)message {
	
    return _message; 
}

- (void)setMessage:(NSString *)aMessage {
	
    if (_message != aMessage) {
		
        [aMessage retain];
        [_message release];
        _message = aMessage;
    }
}

//=========================================================== 
//  logDate 
//=========================================================== 
- (NSDate *)logDate {
	
    return _logDate; 
}
- (void)setLogDate:(NSDate *)aLogDate {
	
    if (_logDate != aLogDate) {
		
        [aLogDate retain];
        [_logDate release];
        _logDate = aLogDate;
    }
}

@end
