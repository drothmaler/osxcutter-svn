//
//  CLDefaultLayout.m
//  CocoLogger
//
//  Created by Thomas Bonk on 27.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import "CLDefaultLayout.h"
#import "CLLayoutProtocol.h"
#import "CLLogEvent.h"


@implementation CLDefaultLayout

- (id)initWithParameters:(NSDictionary*)theParameters {

	if (self = [super init]) {
	
		_showTimestamp = [[theParameters objectForKey:@"ShowTimestamp"] boolValue];
	}
	
	return self;
}

- (NSString*)logEventToString:(CLLogEvent*)aLogEvent {
	
	NSString* logMessage = nil;
	
	if ([self showTimestamp]) {
		
		logMessage = [NSString stringWithFormat:@"%@ | %@ | %@ | %@", [NSDate date],
																      [aLogEvent category],
			                                                          CLPriorityName([aLogEvent prioriyLevel]),
			                                                          [aLogEvent message]];
	}
	else {
		
		logMessage = [NSString stringWithFormat:@"%@ | %@ | %@", [aLogEvent category],
																 CLPriorityName([aLogEvent prioriyLevel]),
																 [aLogEvent message]];
	}
	
	return logMessage;
}

- (void)setShowTimestamp:(BOOL)flShow {
	
	_showTimestamp = flShow;
}

- (BOOL)showTimestamp {
	
	return _showTimestamp;
}

@end
