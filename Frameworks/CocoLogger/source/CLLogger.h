//
//  CLLogger.h
//  CocoLogger
//
//  Created by Thomas Bonk on 28.09.06.
//  Copyright 2006 Thomas Bonk. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@class CLCategory;
@protocol CLAppenderProtocol;
@protocol CLLayoutProtocol;


/** This class provides methods for managing the whole logging mechanism.
 */
@interface CLLogger : NSObject {

	NSMutableDictionary* _categories;
}

#pragma mark Creation

/** This class method returns the shared instance of the class.
 * \return the shared instance of this class.
 */
+ (CLLogger*)sharedInstance;


#pragma mark Initialization

/** This method initializes the logging mechanism. It loads the given
 * logging configuration and instantiates the categories, appenders and layouts.
 * Only the first call to this method will initialize the logging mechanism; any
 * subsequent call will be ignored. It isn't an error situation if the file 
 * doesn't exist.
 * \param aConfigFile The name of a configuration file. The file must be located
 *                    in the directory ~/Library/Preferences
 */ 
- (void)initializeLoggingWithConfigurationFile:(NSString*)aConfigFile;

/** This method initializes the logging mechanism. It parses the given
 * logging configuration and instantiates the categories, appenders and layouts.
 * Only the first call to this method will initialize the logging mechanism; any
 * subsequent call will be ignored. It isn't an error situation if the dictionary
 * is empty.
 * \param aConfig A configuration in a dictionary
 */ 
- (void)initializeLoggingWithConfiguration:(NSDictionary*)aConfig;


#pragma Category related methods

/** This method returns the category with the given name. If the category doesn't
 * exist, it will be created.
 * \param aName The name of the category.
 * \return The category.
 */
- (CLCategory*)categoryWithName:(NSString*)aName;


#pragma Appender related methods

/** This method creates an appender object which is an instance of the class
 * with the given name.
 * \param anAppenderClassName The name of the appender class.
 * \param theAppenderParameters The parameters for the appender.
 * \return The new appender obejct.
 */
- (id<CLAppenderProtocol>)appender:(NSString*)anAppenderClassName 
					withParameters:(NSDictionary*)theAppenderParameters;


#pragma Layout related methods

/** This method creates a layout object which is an instance of the class
 * with the given name.
 * \param aLayoutClassName The name of the layout class.
 * \param theLayoutParameters The parameters for the layout.
 * \return The new layout obejct.
 */
- (id<CLLayoutProtocol>)layout:(NSString*)aLayoutClassName 
				withParameters:(NSDictionary*)theLayoutParameters;



@end
