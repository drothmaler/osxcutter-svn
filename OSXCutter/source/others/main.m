//
//  main.m
//  OSXCutter
//
//  Created by Thomas Bonk on 06.06.07.
//  Copyright Thomas Bonk 2007. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
