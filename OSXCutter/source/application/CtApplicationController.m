//
//  CtApplicationController.m
//  OSXCutter
//
//  Created by Thomas Bonk on 06.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share � to copy, distribute and transmit the work
//      * to Remix � to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import "CtCommon.h"
#import "CtApplicationController.h"
#import "CtMovie.h"
#import "CtCutlist.h"
#import "CtPreferencesModel.h"
#import "CtDecoderTask.h"
#import "ImageAndTextCell.h"


@interface CtApplicationController (private)

#pragma mark Notification Callbacks

- (void)cutlistSelected:(NSNotification*)notification;

#pragma mark Cutting movies

- (void)loadMovieAndPerformCutting:(CtMovie*)aMovie;
- (void)finishedDecodingMovie:(CtMovie*)aMovie;
- (void)finishedCuttingMovie:(CtMovie*)aMovie;
- (void)cutMovies:(id)anObject;
- (BOOL)progressingMovie:(CtMovie*)aMovie;

#pragma mark Utility methods

- (BOOL)targetFolderExists;
- (BOOL)isDivXAvailable;
- (BOOL)isComponentAvailable:(NSString*)aComponentName;
- (NSArray*)availableComponents;

@end


@implementation CtApplicationController

#pragma mark Construction/Destruction

- (id)init {

	if (self = [super init]) {
		
		[[CLLogger sharedInstance] initializeLoggingWithConfigurationFile:@"OSXCutter-Logging.plist"];
		CtLogger = [[CLLogger sharedInstance] categoryWithName:@"OSXCutter"];
				
		_movies = [[NSMutableArray alloc] init];
		_cutlists = [[NSMutableArray alloc] init];
		_cuttingInProgress = NO;
		
		_currentCuttingMovie = [[NSString string] retain];
		_currentCuttingStep  = [[NSString string] retain];
		_minCuttingProgress = 0.0;
		_maxCuttingProgress = 0.0;
		_currentProgress = 0.0;
		_manualCutlistLoadingInProgress = NO;
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(cutlistSelected:)
													 name:CtCutlistSelectedNotification 
												   object:nil];
		
		NSBundle* bundle = [NSBundle mainBundle];
		_errorLoadingCutlistImage = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"statusErrorLoadingCutlist" ofType:@"png"]];
		_noCutlistsImage = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"statusNoCutlists" ofType:@"png"]];
		_noErrorImage = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"statusNoError" ofType:@"png"]];
		_notAMovieImage = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"statusNotAMovie" ofType:@"png"]];
	}
	
	return self;
}

- (void)awakeFromNib {

	NSTableColumn      *tableColumn = nil;
    ImageAndTextCell *imageAndTextCell = nil;
	
    tableColumn = [_moviesTableView tableColumnWithIdentifier:@"STATUS"];
    imageAndTextCell = [[[ImageAndTextCell alloc] init] autorelease];
    [imageAndTextCell setEditable:NO];
    [tableColumn setDataCell:imageAndTextCell];
}

- (void)dealloc {
	
	[[NSNotificationCenter defaultCenter] remove:self];
	
	[_movies release];
	_movies = nil;
	
	[_cutlists release];
	_cutlists = nil;
	
	[_errorLoadingCutlistImage release];
	_errorLoadingCutlistImage = nil;
	[_noCutlistsImage release];
	_noCutlistsImage = nil;
	[_noErrorImage release];
	_noErrorImage = nil;
	[_notAMovieImage release];
	_notAMovieImage = nil;
	
	[super dealloc];
}


#pragma mark Accessors for the attributes

- (NSString*)targetPath {
	
    return [[CtPreferencesModel instance] targetPath];
}

- (void)setTargetPath:(NSString*)value {
	
	[[CtPreferencesModel instance] setTargetPath:value];
}



- (NSMutableArray*)movies {
	
	return _movies;
}

- (NSMutableArray*)cutlists {
	
	return _cutlists;
}


- (NSString*)currentCuttingMovie {
	
    return _currentCuttingMovie;
}

- (void)setCurrentCuttingMovie:(NSString*)value {
	
    if (_currentCuttingMovie != value) {
		
		[value retain];
        [_currentCuttingMovie release];
        _currentCuttingMovie = value;
    }
}

- (NSString*)currentCuttingStep {
	
    return _currentCuttingStep;
}

- (void)setCurrentCuttingStep:(NSString*)value {
	
    if (_currentCuttingStep != value) {
		
		[value retain];
        [_currentCuttingStep release];
        _currentCuttingStep = value;
    }
}

- (long)minCuttingProgress {
	
    return _minCuttingProgress;
}

- (void)setMinCuttingProgress:(long)value {
	
    _minCuttingProgress = value;
}

- (long)maxCuttingProgress {
	
    return _maxCuttingProgress;
}

- (void)setMaxCuttingProgress:(long)value {
	
    _maxCuttingProgress = value;
}

- (long)currentProgress {
	
    return _currentProgress;
}

- (void)setCurrentProgress:(long)value {
	
    _currentProgress = value;
}


#pragma mark Action handlers

- (IBAction)doShowHelp:(id)sender {
	
	NSString* helpFile = [[NSBundle mainBundle] pathForResource:@"OSXCutter" ofType:@"pdf"];
	
	[[NSWorkspace sharedWorkspace] openFile:helpFile];
}

- (IBAction)toggleCutlistDrawer:(id)sender {
	
	[[_mainWindow cutlistDrawer] toggle:sender];
}

- (IBAction)doDeleteFile:(id)sender {

	NSTableView*  tableView    = [_mainWindow movieListView];
	NSIndexSet*   selectedRows = [tableView selectedRowIndexes];
	
	[_movieArrayController removeObjectsAtArrangedObjectIndexes:selectedRows];
}

- (IBAction)doStartCutting:(id)sender {
	
	if (!_cuttingInProgress && [[self movies] count] > 0) {
		
		_cuttingInProgress = YES;
		
		[NSApp beginSheet:_progressPanel
		   modalForWindow:_mainWindow 
			modalDelegate:self 
		   didEndSelector:nil
			  contextInfo:nil];
		[_progressIndicator setUsesThreadedAnimation:YES];
		[_progressIndicator startAnimation:sender];
		
		[self performSelector:@selector(performCutting:) 
					 onThread:[NSThread mainThread]
				   withObject:nil
				waitUntilDone:NO];
		//[NSThread detachNewThreadSelector:@selector(performCutting:) 
		//						 toTarget:self 
		//					   withObject:nil];
	}
}

- (IBAction)doStopCutting:(id)sender {
	
	_cuttingInProgress = NO;
}

- (void)openFilePanelDidEnd:(NSOpenPanel*)panel returnCode:(int)returnCode  contextInfo:(void*)contextInfo {
	
	if (returnCode == NSOKButton) {
		
		[_movieArrayController loadFiles:[panel filenames]];
		//[self application:NSApp openFiles:[panel filenames]];
	}
}

- (IBAction)doOpenFile:(id)sender {
	
	NSOpenPanel* openPanel = [NSOpenPanel openPanel];
	
	[openPanel setCanChooseFiles:YES];
	[openPanel setResolvesAliases:YES];
	[openPanel setAllowsMultipleSelection:YES];
	[openPanel beginSheetForDirectory:nil 
								 file:nil
								types:[NSArray arrayWithObjects:@"AVI", @"OTRKEY", nil]
					   modalForWindow:_mainWindow 
						modalDelegate:self 
					   didEndSelector:@selector(openFilePanelDidEnd:returnCode:contextInfo:) 
						  contextInfo:nil];
}

- (void)openTargetPathPanelDidEnd:(NSOpenPanel*)panel returnCode:(int)returnCode  contextInfo:(void*)contextInfo {
	
	if (returnCode == NSOKButton) {
		
		[self setTargetPath:[[panel filenames] objectAtIndex:0]];
	}
}

- (IBAction)doSelectTargetPath:(id)sender {
	
	NSOpenPanel* openPanel = [NSOpenPanel openPanel];
	
	[openPanel setCanChooseFiles:NO];
	[openPanel setCanChooseDirectories:YES];
	[openPanel setResolvesAliases:YES];
	[openPanel setAllowsMultipleSelection:NO];
	[openPanel beginSheetForDirectory:[self targetPath]
								 file:nil
								types:nil
					   modalForWindow:_mainWindow 
						modalDelegate:self 
					   didEndSelector:@selector(openTargetPathPanelDidEnd:returnCode:contextInfo:) 
						  contextInfo:nil];
}


- (void)openCutlistPanelDidEnd:(NSOpenPanel*)panel returnCode:(int)returnCode  contextInfo:(void*)contextInfo {
	
	if (returnCode == NSOKButton) {
		
		CtMovie* movie = (CtMovie*)contextInfo;
		
		_manualCutlistLoadingInProgress = YES;
		
		[_cutlistsOfSelectedMovieController removeObjects:[movie cutlists]];
		[movie loadCutlistsFromFiles:[panel filenames]];
		[_cutlistsOfSelectedMovieController addObjects:[movie cutlists]];
		
		_manualCutlistLoadingInProgress = NO;
	}
}

- (IBAction)doAddCutlist:(id)sender {
	
	NSTableView*  tableView    = [_mainWindow movieListView];
	NSIndexSet*   selectedRows = [tableView selectedRowIndexes];
	
	if ([selectedRows count] == 1) {
		
		NSOpenPanel* openPanel = [NSOpenPanel openPanel];
		
		[openPanel setCanChooseFiles:YES];
		[openPanel setResolvesAliases:YES];
		[openPanel setAllowsMultipleSelection:YES];
		[openPanel beginSheetForDirectory:nil 
									 file:nil
									types:[NSArray arrayWithObject:@"cutlist"]
						   modalForWindow:_mainWindow 
							modalDelegate:self 
						   didEndSelector:@selector(openCutlistPanelDidEnd:returnCode:contextInfo:) 
							  contextInfo:[[self movies] objectAtIndex:[selectedRows firstIndex]]];
	}
	else {
		
		NSBeep();
	}
}


#pragma mark Notification Callbacks

- (void)cutlistSelected:(NSNotification*)notification {

	if (!_manualCutlistLoadingInProgress) {
		
		CtCutlist* cutlist  = [notification object];
		NSArray*   cutlists = [_cutlistsOfSelectedMovieController arrangedObjects];
	
		[_cutlistsOfSelectedMovieController objectDidBeginEditing:self];
	
		[cutlists makeObjectsPerformSelector:@selector(deselect)];
		[cutlist select];
	
		[_cutlistsOfSelectedMovieController objectDidEndEditing:self];
	}
}


#pragma mark Cutting movies

- (void)performCutting:(id)prevMovie {

	int      index   = 0;
	CtMovie* movie   = nil;
	BOOL     canGoOn = YES;
	
	if (prevMovie != nil) {
	
		int ndx = [[self movies] indexOfObject:prevMovie];
		
		if (ndx != NSNotFound) {
			
			index = ndx + 1;
			
			if (index >= [[self movies] count]) {
				
				canGoOn = NO;
			}
		}
	}
	
	if (canGoOn && ([[self movies] count] > 0)) {
		
		movie = [[self movies] objectAtIndex:index];
		
		[self setCurrentCuttingMovie:[movie filename]];
		
		if ([movie movieFileType] == CtMovieFileTypeOtrKey) {
			
			[self performSelector:@selector(decodeMovie:) 
						 onThread:[NSThread mainThread]
					   withObject:movie
					waitUntilDone:NO];
		}
		else if ([movie movieFileType] != CtMovieFileTypeUnknown) {

			[self performSelector:@selector(loadMovieAndPerformCutting:) 
						 onThread:[NSThread mainThread]
					   withObject:movie
					waitUntilDone:NO];
		}
		else {
		
			[movie setStatus:NotAMovie];
			[CtLogger warn:@"The movie %@ isn't a movie or an OTRKey.", [movie qualifiedFilename]];
			[self fireFinishedCuttingMovie:movie];
		}
	}
	else {
		
		_cuttingInProgress = NO;
		[_progressIndicator stopAnimation:self];
		[NSApp endSheet:_progressPanel];
		[_progressPanel orderOut:nil];
		
		if ([[CtPreferencesModel instance] quitAfterCutting]) {
		
			[NSApp terminate:self];
		}
	}
}

- (void)decodeMovie:(CtMovie*)aMovie {

	CtDecoderTask* decoderTask = [[CtDecoderTask alloc] init];
	
	if ([[[CtPreferencesModel instance] username] length] > 0 && 
		[[[CtPreferencesModel instance] password] length] > 0) {
		
		[_progressIndicator setIndeterminate:NO];
		[decoderTask decode:aMovie andTrigger:self];
	}
	else {

		[CtLogger warn:@"Email or password for decoder not set!"];
		
		[aMovie setStatus:DecodeError];
		[_moviesTableView reloadData];
		[self fireFinishedCuttingMovie:aMovie];
	}
}

- (void)decoder:(CtDecoderTask*)decoderTask fireFinishedDecodingMovie:(CtMovie*)aMovie succeeded:(BOOL)didSucceed {
	
	[_progressIndicator setIndeterminate:YES];
	
	if (_cuttingInProgress) {
		
		if (didSucceed) {
			
			[self performSelector:@selector(finishedDecodingMovie:) 
						 onThread:[NSThread mainThread]
					   withObject:aMovie
					waitUntilDone:NO];
		}
		else {
			
			[aMovie setStatus:DecodeError];
			[_moviesTableView reloadData];
			[self fireFinishedCuttingMovie:aMovie];
		}
	}
	else {
		
		_cuttingInProgress = NO;
		[_progressIndicator stopAnimation:self];
		[NSApp endSheet:_progressPanel];
		[_progressPanel orderOut:nil];
	}
	
	[decoderTask release];
}

- (void)finishedDecodingMovie:(CtMovie*)aMovie {
	
	NSString* qualifiedFilename    = [aMovie qualifiedFilename];
	NSString* newQualifiedFilename = [qualifiedFilename stringByDeletingPathExtension];
	
	if ([[CtPreferencesModel instance] moveOtrKeysToTrash]) {
		
		int tag;
		
		[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceRecycleOperation
													 source:[qualifiedFilename stringByDeletingLastPathComponent]
												destination:@""
													  files:[NSArray arrayWithObject:[qualifiedFilename lastPathComponent]]
													    tag:&tag];
	}
	
	[aMovie setQualifiedFilename:newQualifiedFilename];
	[_moviesTableView reloadData];
	
	[self performSelector:@selector(loadMovieAndPerformCutting:) 
				 onThread:[NSThread mainThread]
			   withObject:aMovie
			waitUntilDone:NO];
}

- (void)loadMovieAndPerformCutting:(CtMovie*)aMovie {
	
	if ([aMovie loadMovie] && [aMovie status] == NoError) {
		
		[aMovie setTargetPath:[self targetPath]];
		
		[NSThread detachNewThreadSelector:@selector(performCutAndTrigger:) 
								 toTarget:aMovie 
							   withObject:self];
	}
	else {
	
		[CtLogger warn:@"Error while cutting movie %@; status is %d.", [aMovie qualifiedFilename], [aMovie status]];
		[self fireFinishedCuttingMovie:aMovie];
	}
	
	[_moviesTableView reloadData];
}

- (void)fireFinishedCuttingMovie:(CtMovie*)aMovie {
	
	[self performSelector:@selector(finishedCuttingMovie:) 
				 onThread:[NSThread mainThread]
			   withObject:aMovie
			waitUntilDone:NO];
}

- (void)finishedCuttingMovie:(CtMovie*)aMovie {
	
	CtMovie* prevMovie = nil;
	
	[self setCurrentCuttingMovie:@""];
	[self setCurrentCuttingStep:@""];
	
	if ([aMovie fileDeleted]) {
		
		[_movieArrayController removeObject:aMovie];
		[_moviesTableView reloadData];
	}
	else {
		
		prevMovie = aMovie;
	}
	
	if (_cuttingInProgress) {
	
		[self performSelector:@selector(performCutting:) 
					 onThread:[NSThread mainThread]
				   withObject:prevMovie
				waitUntilDone:NO];
	}
	else {
	
		_cuttingInProgress = NO;
		[_progressIndicator stopAnimation:self];
		[NSApp endSheet:_progressPanel];
		[_progressPanel orderOut:nil];
	}
}

- (BOOL)progressingMovie:(CtMovie*)aMovie {
		
	[_progressPanel display];
	[_moviesTableView display];
	
	return _cuttingInProgress;
}

- (BOOL)movie:(QTMovie*)movie shouldContinueOperation:(NSString*)op withPhase:(QTMovieOperationPhase)phase atPercent:(NSNumber*)percent withAttributes:(NSDictionary*)attributes {

	[self setCurrentCuttingStep:op];
	[_progressIndicator setIndeterminate:(phase != QTMovieOperationUpdatePercentPhase)];
		
	if (phase == QTMovieOperationUpdatePercentPhase) {
		
		[self setCurrentProgress:(long)([percent doubleValue] * 100.0)];
		[self setCurrentCuttingStep:[NSString stringWithFormat:@"%@ (%d%%)",op, (int)[self currentProgress]]];
		[_progressPanel display];
		[_moviesTableView display];
	}
	
	return _cuttingInProgress;
}


#pragma mark Delegate methods for table views

- (void)tableViewSelectionDidChange:(NSNotification*)aNotification {

	NSTableView* tableView = (NSTableView*)[aNotification object];
	
	if ([[tableView autosaveName] isEqualToString:@"MOVIES_TABLEVIEW"]) {
	
		NSIndexSet* selectedRows = [tableView selectedRowIndexes];
		
		if ([selectedRows count] == 1) {
			
			CtMovie* movie = [_movies objectAtIndex:[selectedRows firstIndex]];
			
			[_cutlistsOfSelectedMovieController removeObjects:_cutlists];
			
			if (   [movie movieFileType] != CtMovieFileTypeUnknown 
				&& [movie movieFileType] != CtMovieFileTypeOtrKey) {
				
				[_cutlistsOfSelectedMovieController addObjects:[movie cutlists]];
			}
		}
		else {
			
			[_cutlistsOfSelectedMovieController removeObjects:_cutlists];
		}
		
		[_moviesTableView display];
	}
}


- (void)tableView:(NSTableView*)aTableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn*)aTableColumn row:(int)rowIndex {
	
	if ([[aTableView autosaveName] isEqualToString:@"MOVIES_TABLEVIEW"]) {
		
		if ([[aTableColumn identifier] isEqualToString:@"STATUS"]) {
		
			CtMovie*            movie = [_movies objectAtIndex:rowIndex];
			ImageAndTextCell* cell  = (ImageAndTextCell*)aCell;
			
			switch ([movie status]) {
			
				case NoError:
					[cell setImage:_noErrorImage];
					break;
					
			    case DecodeError:
				case NotAMovie:
					[cell setImage:_notAMovieImage];
					break;
					
				case NoCutlists:
					[cell setImage:_noCutlistsImage];
					break;
					
				case ErrorLoadingCutlist:
					[cell setImage:_errorLoadingCutlistImage];
					break;
			}
		}
	}
}


#pragma mark Delegate methods for the main window

- (BOOL)validateToolbarItem:(NSToolbarItem*)toolbarItem {
	
	BOOL rc = NO;
	
	if ([toolbarItem tag] == TBID_OPEN_FILE) {
		
		rc = !_cuttingInProgress;
	}
	else if ([toolbarItem tag] == TBID_DELETE_FILE) {
		
		NSTableView*  tableView    = [_mainWindow movieListView];
		NSIndexSet*   selectedRows = [tableView selectedRowIndexes];
		
		rc = [selectedRows count] > 0 && !_cuttingInProgress;
	}
	else if ([toolbarItem tag] == TBID_START_CUTTING) {
	
		rc = !_cuttingInProgress && [[self movies] count] > 0;
	}
	else if ([toolbarItem tag] == TBID_STOP_CUTTING) {
	
		rc = _cuttingInProgress;
	}
	else if ([toolbarItem tag] == TBID_TOGGLE_DRAWER) {
		
		rc = YES;
	}
	
	return rc;
}


#pragma mark Delegate methods of the application

- (BOOL)application:(NSApplication*)theApplication openFile:(NSString*)filename {
	
	[self application:theApplication openFiles:[NSArray arrayWithObject:filename]];
	
	return YES;
}

- (void)application:(NSApplication*)theApplication openFiles:(NSArray*)filenames {
	
	[_movieArrayController loadFiles:filenames];
	
	if ([[CtPreferencesModel instance] startCuttingAfterOpenFiles]) {
		
		[self performSelector:@selector(doStartCutting:) 
					 onThread:[NSThread mainThread]
				   withObject:self
				waitUntilDone:NO];
	}
}

- (void)forceQuit:(id)sender {

	[NSApp terminate:sender];
}

- (void)applicationDidFinishLaunching:(NSNotification*)aNotification {
	
	if (![self isDivXAvailable]) {
		
		[CtLogger fatal:@"DivX is not installed. OSXCutter will be terminated."];
		
		NSAlert* alert = [NSAlert alertWithMessageText:NSLocalizedString(@"DivX is not installed. OSXCutter will be terminated. Please install DivX and try it again. For more information concerning DivX see http://www.divx.com", nil)
										 defaultButton:nil 
									   alternateButton:nil 
										   otherButton:nil 
							 informativeTextWithFormat:NSLocalizedString(@"Please install DivX and try it again. For more information concerning DivX see http://www.divx.com", nil)];
		
		[alert beginSheetModalForWindow:_mainWindow 
						  modalDelegate:self 
						 didEndSelector:@selector(forceQuit:) 
							contextInfo:nil];
	}
	
	if (![self targetFolderExists]) {
	
		[CtLogger info:@"Target path <%@> doesn't exist anymore. It will be set to <%@>.", [[CtPreferencesModel instance] targetPath], NSHomeDirectory()];
		[[CtPreferencesModel instance] setTargetPath:NSHomeDirectory()];
	}
	
	[QTMovie initialize];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)theApplication {

	return YES;
}


#pragma mark Utility methods

- (BOOL)targetFolderExists {

	BOOL           exists      = YES;
	BOOL           isDir       = YES;
	NSFileManager* fileManager = [NSFileManager defaultManager];
	
	exists = [fileManager fileExistsAtPath:[[CtPreferencesModel instance] targetPath] isDirectory:&isDir];
	
	return exists && isDir;
}

- (BOOL)isDivXAvailable {

	BOOL available = [self isComponentAvailable:@"DivX"];
	
	[CtLogger info:@"Checking for DivX. DivX is installed: %@.", (available ? @"YES" : @"NO")];
	
	return available;
}

- (BOOL)isComponentAvailable:(NSString*)aComponentName {
	
	NSArray* components = [self availableComponents];
	
	for (int i = 0, n = [components count]; i < n; i++ ) {
	
		NSDictionary* component = [components objectAtIndex:i];
		
		if ([[component valueForKey:@"name"] isEqualToString:aComponentName]) {
			
			return YES;
		}
	}
	
	return NO;
}

- (NSArray*)availableComponents {
	
	NSMutableArray *array = [NSMutableArray array];
	
	ComponentDescription cd;
	Component c;
	
	cd.componentType = MovieExportType;
	cd.componentSubType = 0;
	cd.componentManufacturer = 0;
	cd.componentFlags = canMovieExportFiles;
	cd.componentFlagsMask = canMovieExportFiles;
	
	while ((c = FindNextComponent(c, &cd))) {
		
		Handle name = NewHandle(4);
		ComponentDescription exportCD;
		
		if (GetComponentInfo(c, &exportCD, name, nil, nil) == noErr) {
			
			unsigned char *namePStr = (unsigned char*)*name;
			NSString *nameStr = [[NSString alloc] initWithBytes:&namePStr[1] length:namePStr[0] encoding:NSUTF8StringEncoding];
			
			NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
				nameStr, @"name",
				[NSData dataWithBytes:&c length:sizeof(c)], @"component",
				[NSNumber numberWithLong:exportCD.componentType], @"type",
				[NSNumber numberWithLong:exportCD.componentSubType], @"subtype",
				[NSNumber numberWithLong:exportCD.componentManufacturer], @"manufacturer",
				nil];
			[array addObject:dictionary];
			[nameStr release];
		}
		
		DisposeHandle(name);
	}
	
	return array;
}

@end
