//
//  CtMovieArrayController.m
//  OSXCutter
//
//  Created by Thomas Bonk on 07.06.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import "CtCommon.h"
#import "CtMovieArrayController.h"
#import "CtMovie.h"


@implementation CtMovieArrayController

#pragma mark Utility methods for drag and drop

- (NSArray*)droppedFilenamesFromDraggingInfo:(id<NSDraggingInfo>)info {
	
	NSPasteboard* pboard           = [info draggingPasteboard];
	NSArray*      droppedFilenames = (NSArray*)[pboard propertyListForType:NSFilenamesPboardType];
	
	return droppedFilenames;
}

- (BOOL)validatedFiles:(NSArray*)filenames {

	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	for (int i = 0, n = [filenames count]; i < n; i++) {
		
		NSString* filename = [filenames objectAtIndex:i];
		BOOL      isDir;
		
		if (   ![fileManager fileExistsAtPath:filename isDirectory:&isDir] 
			|| isDir
			|| ![fileManager isReadableFileAtPath:filename]) {
		
			[CtLogger warn:@"Invalid file %@", filename];
			return NO;
		}
	}
	
	return YES;
}


#pragma mark Utility methods

- (void)loadFilesAsync:(NSDictionary*)parameters {
	
	[QTMovie enterQTKitOnThread];
	
	NSArray*  filenames      = [parameters objectForKey:@"FILENAMES"];
	NSNumber* rowNum         = [parameters objectForKey:@"ROW"];
	bool      insertAfterRow = (rowNum != nil);
	int       row            = [rowNum intValue];
	
	if ([filenames count] > 0 && [self validatedFiles:filenames]) {
				
		for (int i = 0, n = [filenames count]; i < n; i++) {
			
			CtMovie* aMovie = [[CtMovie alloc] initWithQualifiedFilename:[filenames objectAtIndex:i]];
			
			if (insertAfterRow) {
				
				[self insertObject:aMovie atArrangedObjectIndex:(row + i)];
			}
			else {
				
				[self addObject:aMovie];
			}
			[aMovie release];
		}
	}
	
	[QTMovie exitQTKitOnThread];
}

- (BOOL)loadFiles:(NSArray*)filenames {
	
	NSDictionary* parameters = [NSDictionary dictionaryWithObject:filenames forKey:@"FILENAMES"];
	
	[self loadFilesAsync:parameters];
	//[NSThread detachNewThreadSelector:@selector(loadFilesAsync:) 
	//						 toTarget:self 
	//					   withObject:parameters];
    
    return YES;
}

- (BOOL)loadFiles:(NSArray*)filenames andInsertAfterRow:(int)row {
	
	NSDictionary* parameters = [NSDictionary dictionaryWithObjectsAndKeys:filenames, @"FILENAMES", [NSNumber numberWithInt:row], @"ROW", nil];
	
	[self loadFilesAsync:parameters];
	//[NSThread detachNewThreadSelector:@selector(loadFilesAsync:) 
	//						 toTarget:self 
	//					   withObject:parameters];
	
    return YES;
}

#pragma mark Drag and drop related methods

- (NSDragOperation)tableView:(NSTableView*)tv 
				validateDrop:(id<NSDraggingInfo>)info 
				 proposedRow:(int)row 
	   proposedDropOperation:(NSTableViewDropOperation)op {
	
	if (![self validatedFiles:[self droppedFilenamesFromDraggingInfo:info]]) {
		
		return NSDragOperationNone;
	}
	else {
		
		if (op == NSTableViewDropOn) {
		
			[tv setDropRow:(row+1) dropOperation:NSTableViewDropAbove];
		}
	
		return NSDragOperationCopy;
	}
}

- (BOOL)tableView:(NSTableView*)tv 
	   acceptDrop:(id<NSDraggingInfo>)info 
			  row:(int)row 
	dropOperation:(NSTableViewDropOperation)op {
	
    NSArray*      droppedFilenames = [self droppedFilenamesFromDraggingInfo:info];
	BOOL          rc               = NO;
	
	rc = [self loadFiles:droppedFilenames andInsertAfterRow:row];
	
	[tv reloadData];
    
    return rc;
}

@end
