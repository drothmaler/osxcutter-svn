//
//  CtPreferencesModel.m
//  OSXCutter
//
//  Created by Thomas Bonk on 25.09.07.
//  Copyright 2007 Thomas Bonk. All rights reserved.
//  
//  Creative Commons
//  Attribution-Noncommercial-Share Alike 2.0 Germany
//  You are free:
//      * to Share — to copy, distribute and transmit the work
//      * to Remix — to adapt the work
//  
//  Under the following conditions:
//  
//      * Attribution. You must attribute the work in the manner specified by 
//        the author or licensor (but not in any way that suggests that they 
//        endorse you or your use of the work).
//      * Noncommercial. You may not use this work for commercial purposes.
//      * Share Alike. If you alter, transform, or build upon this work, you may
//        distribute the resulting work only under the same or similar license 
//        to this one.
//      * For any reuse or distribution, you must make clear to others the 
//        license terms of this work. The best way to do this is with a link to 
//        this web page.
//      * Any of the above conditions can be waived if you get permission from 
//        the copyright holder.
//      * Nothing in this license impairs or restricts the author's moral rights.
//  
//  
//  Your fair dealing and other rights are in no way affected by the above.
//  This is a human-readable summary of the Legal Code (the full license):
//  http://creativecommons.org/licenses/by-nc-sa/2.0/de/legalcode
//


#import "EMKeychainProxy.h"
#import "CtPreferencesModel.h"


static CtPreferencesModel* _instance = nil;

static NSString* DVKTargetPath			        = @"DVKTargetPath";
static NSString *DVKDeleteOriginalFilesAfterCut = @"DVKDeleteOriginalFilesAfterCut";
static NSString* DVKExportExtAviToAvi	        = @"DVKExportExtAviToAvi";
static NSString* DVKExportExtHqAviToAvi	        = @"DVKExportExtHqAviToAvi";
static NSString* DVKExportExtMp4ToMp4	        = @"DVKExportExtMp4ToMp4";
static NSString* DVKNetworkTimeout				= @"DVKNetworkTimeout";
static NSString* DVKDecoderUsername				= @"DVKDecoderUsername";
static NSString* DVKVerifyOtrKeys				= @"DVKVerifyOtrKeys";
static NSString* DVKForceOverwritingOfFiles		= @"DVKForceOverwritingOfFiles";
static NSString* DVKMoveOtrKeysToTrash			= @"DVKMoveOtrKeysToTrash";
static NSString* DVKLoadCutlistsFromServer	    = @"DVKLoadCutlistsFromServer";
static NSString* DVKCutlistServer				= @"DVKCutlistServer";
static NSString* DVKStartCuttingAfterOpenFiles  = @"DVKStartCuttingAfterOpenFiles";
static NSString* DVKQuitAfterCutting			= @"DVKQuitAfterCutting";


@implementation CtPreferencesModel

#pragma mark Initialization / Deallocation

+ (CtPreferencesModel*)instance {
	
	CtPreferencesModel* ins;
	
	if (_instance == nil) {
		
		ins = [[CtPreferencesModel alloc] init];
	}
	else {
		
		ins = _instance;
	}
	
	return ins;
}

+ (void)initialize {
	
	NSMutableDictionary* defaultValues = [NSMutableDictionary dictionary];
	
	[defaultValues setObject:NSHomeDirectory() forKey:DVKTargetPath];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKDeleteOriginalFilesAfterCut];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKExportExtAviToAvi];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKExportExtHqAviToAvi];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKExportExtMp4ToMp4];
	[defaultValues setObject:[NSNumber numberWithInt:30] forKey:DVKNetworkTimeout];
	[defaultValues setObject:@"" forKey:DVKDecoderUsername];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKVerifyOtrKeys];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKForceOverwritingOfFiles];
	[defaultValues setObject:[NSNumber numberWithBool:YES] forKey:DVKMoveOtrKeysToTrash];
	[defaultValues setObject:[NSNumber numberWithBool:YES] forKey:DVKLoadCutlistsFromServer];
	[defaultValues setObject:@"http://www.cutlist.at" forKey:DVKCutlistServer];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKStartCuttingAfterOpenFiles];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:DVKQuitAfterCutting];
	
	[[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
}

- (id)init {
	
	if (_instance == nil) {
		
		if (self = [super init]) {
		
			_instance = self;
			
			if ([self verifyOtrKeys]) {
			
				[self setVerifyOtrKeys:NO];
			}
		}
	}
	
	return _instance;
}

- (void)dealloc {
	
	// do nothing
}


#pragma mark Accessors for the attributes

- (NSString*)targetPath {
	
	return [[NSUserDefaults standardUserDefaults] stringForKey:DVKTargetPath];
}

- (void)setTargetPath:(NSString*)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:value forKey:DVKTargetPath];
}

- (BOOL)deleteOriginalFilesAfterCut {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKDeleteOriginalFilesAfterCut] boolValue];
}

- (void)setDeleteOriginalFilesAfterCut:(BOOL)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKDeleteOriginalFilesAfterCut];
}

- (BOOL)exportExtAviToAvi {

	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKExportExtAviToAvi] boolValue];
}

- (void)setExportExtAviToAvi:(BOOL)value {

	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKExportExtAviToAvi];
}

- (BOOL)exportExtHqAviToAvi {

	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKExportExtHqAviToAvi] boolValue];
}

- (void)setExportExtHqAviToAvi:(BOOL)value {

	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKExportExtHqAviToAvi];
}

- (BOOL)exportExtMp4ToMp4 {

	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKExportExtMp4ToMp4] boolValue];
}

- (void)setExportExtMp4ToMp4:(BOOL)value {

	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKExportExtMp4ToMp4];
}


- (int)networkTimeout {

	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKNetworkTimeout] intValue];
}

- (void)setNetworkTimeout:(int)timeout {

	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:timeout] forKey:DVKNetworkTimeout];
}


- (NSString*)username {
	
	return [[NSUserDefaults standardUserDefaults] objectForKey:DVKDecoderUsername];
}

- (void)setUsername:(NSString*)newUsername {

	if (![newUsername isEqualToString:[self username]]) {
			
		EMGenericKeychainItem* item = [[EMKeychainProxy sharedProxy] genericKeychainItemForService:@"OSXCutter" withUsername:[self username]];
		
		[item setUsername:newUsername];
		[[NSUserDefaults standardUserDefaults] setObject:newUsername forKey:DVKDecoderUsername];
	}
}


- (NSString*)password {
		
	EMGenericKeychainItem* item = [[EMKeychainProxy sharedProxy] genericKeychainItemForService:@"OSXCutter" withUsername:[self username]];
	
	return [item password];
}

- (void)setPassword:(NSString*)newPassword {
		
	EMGenericKeychainItem* item = [[EMKeychainProxy sharedProxy] genericKeychainItemForService:@"OSXCutter" withUsername:[self username]];
	
	if (item == nil) {
		
		item = [[EMKeychainProxy sharedProxy] addGenericKeychainItemForService:@"OSXCutter" 
																  withUsername:[self username]
																	  password:newPassword];
	}
	else {
		
		[item setPassword:newPassword];
	}
}


- (BOOL)verifyOtrKeys {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKVerifyOtrKeys] boolValue];
}

- (void)setVerifyOtrKeys:(BOOL)value {

	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKVerifyOtrKeys];
}


- (BOOL)forceOverwritingOfFiles {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKForceOverwritingOfFiles] boolValue];
}

- (void)setForceOverwritingOfFiles:(BOOL)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKForceOverwritingOfFiles];
}


- (BOOL)moveOtrKeysToTrash {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKMoveOtrKeysToTrash] boolValue];
}

- (void)setMoveOtrKeysToTrash:(BOOL)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKMoveOtrKeysToTrash];
}


- (BOOL)loadCutlistsFromServer {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKLoadCutlistsFromServer] boolValue];
}

- (void)setLoadCutlistsFromServer:(BOOL)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKLoadCutlistsFromServer];
}


- (NSString*)cutlistServer {
	
	return [[NSUserDefaults standardUserDefaults] objectForKey:DVKCutlistServer];
}

- (void)setCutlistServer:(NSString*)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:value forKey:DVKCutlistServer];
}


- (BOOL)startCuttingAfterOpenFiles {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKStartCuttingAfterOpenFiles] boolValue];
}

- (void)setStartCuttingAfterOpenFiles:(BOOL)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKStartCuttingAfterOpenFiles];
}

- (BOOL)quitAfterCutting {
	
	return [[[NSUserDefaults standardUserDefaults] objectForKey:DVKQuitAfterCutting] boolValue];
}

- (void)setQuitAfterCutting:(BOOL)value {
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:DVKQuitAfterCutting];
}

@end
